package questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import userinterface.SearchCoursePage;


public class Answer implements Question<String> {
     public static Answer toThe() {
        return new Answer();
    }


    @Override
    public String answeredBy(Actor actor) {
        return Text.of(SearchCoursePage.NAME_COURSE).viewedBy(actor).asString();
    }
}
